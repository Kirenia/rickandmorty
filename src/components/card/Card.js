import React from 'react';
//import { Link } from 'react-router-dom';
import styles from './Card.module.css'

function Card(props) {

    const { card } = props;

    return (

        <div className={styles.css}>

            <img src={card.image} alt={card.name} />


            <h4> {card.name}</h4>

            <p>Status: {card.status}</p>
            <div>
                <p>Status: <span>{card.status}</span></p>
                <p>Species: <span>{card.species}</span></p>
                <p>Gender: <span>{card.gender}</span></p>

                <div>

                    <button onClick={() => props.goToCard(card.id)} >More..</button>

                </div>
            </div>

        </div>
    );

}

export default Card;