import React from 'react';


class CardDetail extends React.Component {

    state = {
        character: {},


    }

    componentDidMount() {
        var id1 = this.props.match.params.id;
        console.log(id1);

        fetch(`https://rickandmortyapi.com/api/character/${id1}`).then(resp => resp.json())
            .then(resp => {

                this.setState({

                    character: resp

                });
            }).catch(error => {
                console.log(error);
            });

    }



    render() {
        var myCharacter = null;
        myCharacter = this.state.character;
   
        if (Object.keys(myCharacter).length === 0 ) {

            myCharacter = <p>Page loading ...</p>
        }
        else {


            (myCharacter = <div>
                <h2>Character Details: </h2>
                <img src={myCharacter.image} alt="" />
                <h3>Name: {myCharacter.name}</h3>
                <h3>Race : {myCharacter["species"]}</h3>
                <h3> Location-Url: {this.state.character["url"]}</h3>
                <div>
                    {this.state.character["episode"].map((episode,index)=>(<ul key={index} >Episode :{episode}</ul>))
                    })}
                </div>
                
                 </div>)


        }


        return (

            <React.Fragment>
                {myCharacter}
               
            </React.Fragment>


        );
    }





}

// TODO: Complete the page by building a "Profile" like display for the character
// The character should have his or her photo displayed with their name and race
// Also make provision for the current location and other basic information
// Be creative and make up your own designs.
// Use the React router to extract the ID of the character and make a request to the API to get the current character information.
// Be sure to display a loader while you wait for the data be received from the API call.


export default CardDetail;