import React from 'react';
import styles from './CardList.module.css';
import Card from '../card/Card';
import Header from '../header/Header';





//import { Link } from 'react-router-dom';


class CardList extends React.Component {
    state = {

        cards: [],
       
    };

    //Executes when the HTML has been Loaded into the Dom.
    componentDidMount() {

        fetch('https://rickandmortyapi.com/api/character').then(resp => resp.json())
        .then(resp => {
            this.setState({
                 cards: [...resp.results] 
         
                });
            }).catch(error =>{ console.log(error);
                });
 

    }
handleCardNavigation(id){


 console.log("Navigate to the card with id:" + id);
//    return withRouter("/card-detail");
     this.props.history.push(`card-detail/${id}`);

         }

    render() {
        
        //Declare the output that will be rendered.
        let cardComponents = null;
 
        //Check if we have any cards yet.
        if (this.state.cards.length > 0) {
            // Build an array of Card components.
            cardComponents = this.state.cards.map(card => {
                return <Card card={card} goToCard={(id) => this.handleCardNavigation(id)} key={card.id}/>
                      
            });
        }
        else {
              //Show a loading message while waiting for the api. 
            cardComponents = <p>Loading Rick ...</p>
        }
      

        return (
          
            <React.Fragment>
                <Header/>
                <div className="container">
              
                <div className={styles.CardList}
                >  {cardComponents}
                 </div>
                </div>
            </React.Fragment>


        )
    }


}

export default CardList;